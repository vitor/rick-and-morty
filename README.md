# Rick and Morty

## What is this?

This is an Angular project that consumes the [Rick and Morty REST API][api-link]

## Running

Clone the project and go to the project path.

### Docker

Run as a Docker Image:

```zsh
docker build --pull --rm -f "Dockerfile" -t rickandmorty:latest "."
```

### Traditional

Requisites: Node, Angular and a package manager.


[Npm][npm-link] is used here but you can use other options as [yarn][yarn-link] or [pnpm][pnpm-link](recommended).

Install it's dependencies and run with:

```shell
npm install && ng serve
```

To see it working go to your browser on:

```browser
localhost:4200
```

Enjoy 😉

## Screenshot

![screenshot][screenshot]

## Copyright

Rick and Morty is created by Justin Roiland and Dan Harmon for Adult Swim. The data and images are used without claim of ownership and belong to their respective owners.

This application is open source and uses a [Creative Commons CC0 1.0 Universal license][license-link].

[api-link]: https://rickandmortyapi.com/
[npm-link]: https://www.npmjs.com/
[yarn-link]: https://yarnpkg.com/
[pnpm-link]: https://pnpm.io/
[screenshot]: ./screenshots/theming.png
